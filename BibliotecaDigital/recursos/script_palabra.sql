/* Generating data for Database: proyecto  */


/* Generating data for table public.palabra_clave... */

INSERT INTO "public"."palabra_clave"("nombre","descripcion") 
VALUES('java','lenguaje de programación java');
INSERT INTO "public"."palabra_clave"("nombre","descripcion") 
VALUES('c++','lenguaje de programación c++');
INSERT INTO "public"."palabra_clave"("nombre","descripcion") 
VALUES('postgres','motor de base de datos');
INSERT INTO "public"."palabra_clave"("nombre","descripcion") 
VALUES('mysql','motor de base de datos');
INSERT INTO "public"."palabra_clave"("nombre","descripcion") 
VALUES('oracol','motor de base de datos');
INSERT INTO "public"."palabra_clave"("nombre","descripcion") 
VALUES('oz','lenguaje de programación oz');
INSERT INTO "public"."palabra_clave"("nombre","descripcion") 
VALUES('emacs','editor, compilador.');
INSERT INTO "public"."palabra_clave"("nombre","descripcion") 
VALUES('editor','herramienta que permite modificar el contenido de archivos.');
INSERT INTO "public"."palabra_clave"("nombre","descripcion") 
VALUES('latex','lenguaje de programacion para generar documentos.');
INSERT INTO "public"."palabra_clave"("nombre","descripcion") 
VALUES('software','comprende el conjunto de los componentes lógicos necesarios que hacen posible la realización de tareas específicas');
INSERT INTO "public"."palabra_clave"("nombre","descripcion") 
VALUES('hardware','corresponde a todas las partes tangibles de una computadora: sus componentes eléctricos, electrónicos, electromecánicos y mecánicos.');
INSERT INTO "public"."palabra_clave"("nombre","descripcion") 
VALUES('programación','es un idioma artificial diseñado para expresar computaciones que pueden ser llevadas a cabo por máquinas como las computadoras.');
